
cd /D "%~dp0"

sfdx force:org:create --setdefaultusername -v TrailheadDevHub -f ../config//project-scratch-def.json --setalias current-scratch-org && ^
sfdx force:org:list && ^
sfdx force:package:install -p 04t1Y000001E0g2 -w 30 && ^
sfdx force:source:push
