:: this file is used to extract data from a scratch org. This data can then be used and tweaked to create a data load script to help initialize scratch orgs with data

:: EXTRACT Ecm_Configuration and Ecm_Component_Settings
sfdx force:data:tree:export -q "SELECT ID, Name, DeveloperName, SobjectType FROM RecordType" && ^
sfdx force:data:tree:export --query=soql/EcmConfiguration.soql
